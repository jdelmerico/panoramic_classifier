label_path = '/data/panoramic_classifier/gt_edited/market/';
smooth_path = '/data/panoramic_classifier/gt_smooth/market/';
D = dir([label_path,'*.png']);
for i = 1:size(D,1)
    img = imread([label_path,D(i).name]);
    smooth = medfilt2(img,[10 10]);
    imwrite(smooth,[smooth_path,D(i).name],'png');
end

label_path = '/data/panoramic_classifier/gt_edited/marketPartial/';
smooth_path = '/data/panoramic_classifier/gt_smooth/marketPartial/';
D = dir([label_path,'*.png']);
for i = 1:size(D,1)
    img = imread([label_path,D(i).name]);
    smooth = medfilt2(img,[10 10]);
    imwrite(smooth,[smooth_path,D(i).name],'png');
end

label_path = '/data/panoramic_classifier/gt_edited/main_street/';
smooth_path = '/data/panoramic_classifier/gt_smooth/main_street/';
D = dir([label_path,'*.png']);
for i = 1:size(D,1)
    img = imread([label_path,D(i).name]);
    smooth = medfilt2(img,[10 10]);
    imwrite(smooth,[smooth_path,D(i).name],'png');
end