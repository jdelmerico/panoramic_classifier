#!/usr/local/bin/perl

$fname = "/home/jad12/Projects/panoramic_classifier/adaboost/training_list.txt";
$index = 0;
open (OUTFILE, ">$fname");

# marketPartial
$im_prefix = "/data/panoramic_classifier/images/marketPartial/marketPartial_frame_";
$gt_prefix = "/data/panoramic_classifier/gt_smooth/marketPartial/marketPartial_frame_GT_";
$min_idx = 0;
$max_idx = 79;

foreach my $i ($min_idx..$max_idx) {
    $imfile = sprintf("%s%04d.png",$im_prefix,$i);
    $gtfile = sprintf("%s%04d.png",$gt_prefix,$i);
    print OUTFILE "$index,$gtfile,$imfile\n";
    $index++;
}


# main_street
$im_prefix = "/data/panoramic_classifier/images/main_street/main_street_frame_";
$gt_prefix = "/data/panoramic_classifier/gt_smooth/main_street/main_street_frame_GT_";
$min_idx = 0;
$max_idx = 97;

foreach my $i ($min_idx..$max_idx) {
    $imfile = sprintf("%s%04d.png",$im_prefix,$i);
    $gtfile = sprintf("%s%04d.png",$gt_prefix,$i);
    print OUTFILE "$index,$gtfile,$imfile\n";
    $index++;
}

close (OUTFILE); 
