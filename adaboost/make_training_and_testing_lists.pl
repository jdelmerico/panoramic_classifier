#!/usr/local/bin/perl
use List::Util qw/shuffle/;

# Training
$fname = "/home/jad12/Projects/panoramic_classifier/adaboost/training_list.txt";
$index = 0;
open (OUTFILE, ">$fname");

# market
@market_idx = shuffle 0..113;
@market_train = @market_idx[0..56];
@market_test = @market_idx[57..113];
$px_prefix = "/data/panoramic_classifier/pixel_coords/market/market_pix_coords_";
$im_prefix = "/data/panoramic_classifier/images/market/market_frame_";
$gt_prefix = "/data/panoramic_classifier/gt_smooth/market/market_frame_GT_";

foreach my $i (@market_train) {
    $pxfile = sprintf("%s%04d.txt",$px_prefix,$i);
    $imfile = sprintf("%s%04d.png",$im_prefix,$i);
    $gtfile = sprintf("%s%04d.png",$gt_prefix,$i);
    print OUTFILE "$index,$pxfile,$gtfile,$imfile\n";
    $index++;
}


# main_street
@main_street_idx = shuffle 0..97;
@main_street_train = @main_street_idx[0..48];
@main_street_test = @main_street_idx[49..97];
$px_prefix = "/data/panoramic_classifier/pixel_coords/market/main_street_pix_coords_";
$im_prefix = "/data/panoramic_classifier/images/main_street/main_street_frame_";
$gt_prefix = "/data/panoramic_classifier/gt_smooth/main_street/main_street_frame_GT_";

foreach my $i (@main_street_train) {
    $pxfile = sprintf("%s%04d.txt",$px_prefix,$i);
    $imfile = sprintf("%s%04d.png",$im_prefix,$i);
    $gtfile = sprintf("%s%04d.png",$gt_prefix,$i);
    print OUTFILE "$index,$pxfile,$gtfile,$imfile\n";
    $index++;
}

close (OUTFILE); 


# Validation
$fname = "/home/jad12/Projects/panoramic_classifier/adaboost/validation_list_1.sh";
$exec_name = "/home/jad12/Projects/lab_repo/trunk/vpml_combo/src/boosting/programs/maintest";
$adb_name = "/home/jad12/Projects/panoramic_classifier/adaboost/panoramic_classifier_pts.adb";

$in_prefix = "/data/panoramic_classifier/images/market/market_frame_";
$prob_prefix = "/data/panoramic_classifier/prob_pts/market/market_frame_p_";

open (OUTFILE, ">$fname");
print OUTFILE "#!/bin/bash\n";
foreach my $i (@market_test[0..28]) {
    $infile = sprintf("%s%04d.png",$in_prefix,$i);
    $outfile = sprintf("%s%04d.png",$prob_prefix,$i);
    print OUTFILE "$exec_name $adb_name $infile \"$outfile\"\n";
}
close (OUTFILE); 

$fname = "/home/jad12/Projects/panoramic_classifier/adaboost/validation_list_2.sh";
open (OUTFILE, ">$fname");
print OUTFILE "#!/bin/bash\n";
foreach my $i (@market_test[29..56]) {
    $infile = sprintf("%s%04d.png",$in_prefix,$i);
    $outfile = sprintf("%s%04d.png",$prob_prefix,$i);
    print OUTFILE "$exec_name $adb_name $infile \"$outfile\"\n";
}
close (OUTFILE); 


$fname = "/home/jad12/Projects/panoramic_classifier/adaboost/validation_list_3.sh";
$exec_name = "/home/jad12/Projects/lab_repo/trunk/vpml_combo/src/boosting/programs/maintest";
$adb_name = "/home/jad12/Projects/panoramic_classifier/adaboost/panoramic_classifier_pts.adb";

$in_prefix = "/data/panoramic_classifier/images/main_street/main_street_frame_";
$prob_prefix = "/data/panoramic_classifier/prob_pts/main_street/main_street_frame_p_";

open (OUTFILE, ">$fname");
print OUTFILE "#!/bin/bash\n";
foreach my $i (@main_street_test[0..24]) {
    $infile = sprintf("%s%04d.png",$in_prefix,$i);
    $outfile = sprintf("%s%04d.png",$prob_prefix,$i);
    print OUTFILE "$exec_name $adb_name $infile \"$outfile\"\n";
}
close (OUTFILE); 

$fname = "/home/jad12/Projects/panoramic_classifier/adaboost/validation_list_4.sh";
open (OUTFILE, ">$fname");
print OUTFILE "#!/bin/bash\n";
foreach my $i (@main_street_test[25..48]) {
    $infile = sprintf("%s%04d.png",$in_prefix,$i);
    $outfile = sprintf("%s%04d.png",$prob_prefix,$i);
    print OUTFILE "$exec_name $adb_name $infile \"$outfile\"\n";
}
close (OUTFILE); 
