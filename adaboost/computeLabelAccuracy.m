function image_stats = computeLabelAccuracy(prob, gt, threshold)
% prob and gt are uint8 image matrices loaded from disk
% threshold is between 0.0 and 1.0
% image_stats is a struct with fields for tp, tn, fp, fn, precision,
% recall, accuracy, true neg rate (tnr), false pos rate (fpr), f-score 

image_stats = struct;
prob = double(prob)/255;
lab = uint8(prob > threshold) * 255;

[NR,NC] = size(lab);

tp = 0;
tn = 0;
fp = 0;
fn = 0;

for r = 1:NR
    for c = 1:NC
        if gt(r,c) == 127
            continue;
        elseif gt(r,c) == 255 && lab(r,c) == 255
            tp = tp + 1;
        elseif gt(r,c) == 255 && lab(r,c) == 0
            fn = fn + 1;
        elseif gt(r,c) == 0 && lab(r,c) == 255
            fp = fp + 1;
        elseif gt(r,c) == 0 && lab(r,c) == 0
            tn = tn + 1;
        end
    end
end

image_stats.tp = tp;
image_stats.tn = tn;
image_stats.fp = fp;
image_stats.fn = fn;
image_stats.precision = tp / (tp + fp);
image_stats.recall = tp / (tp + fn);
image_stats.tnr = tn / (fp + tn);
image_stats.fpr = fp / (fp + tn);
image_stats.accuracy = (tp + tn) / (tp + fp + tn + fn);
image_stats.fscore = (2 * image_stats.precision * image_stats.recall)/(image_stats.precision + image_stats.recall);

end