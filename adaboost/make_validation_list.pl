#!/usr/local/bin/perl
$fname = "/home/jad12/Projects/panoramic_classifier/adaboost/validation_list.sh";
$exec_name = "/home/jad12/Projects/lab_repo/trunk/vpml_combo/src/boosting/programs/maintest";
$adb_name = "/home/jad12/Projects/panoramic_classifier/adaboost/panoramic_classifier_no_polar.adb";

$in_prefix = "/data/panoramic_classifier/images/market/market_frame_";
$prob_prefix = "/data/panoramic_classifier/prob_no_polar/market/market_frame_p_";
$idx = 0;
$max_idx = 113;

open (OUTFILE, ">$fname");
print OUTFILE "#!/bin/bash\n";
foreach my $i ($idx..$max_idx) {
    $infile = sprintf("%s%04d.png",$in_prefix,$i);
    $outfile = sprintf("%s%04d.png",$prob_prefix,$i);
    print OUTFILE "$exec_name $adb_name $infile \"$outfile\"\n";
}
close (OUTFILE); 
