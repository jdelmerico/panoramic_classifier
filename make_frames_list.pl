#!/usr/local/bin/perl
$fname = "/home/jad12/Projects/panoramic_classifier/marketPartial_list.sh";
$exec_name = "/home/jad12/Projects/panoramic_classifier/build/grabcut";
$in_prefix = "/data/panoramic_classifier/images/marketPartial/marketPartial_frame_";
$out_prefix = "/data/panoramic_classifier/gt/marketPartial/marketPartial_frame_GT_";
$idx = 61;
$max_idx = 79;

open (OUTFILE, ">$fname");
print OUTFILE "#!/bin/bash\n";
foreach my $i ($idx..$max_idx) {
    $infile = sprintf("%s%04d.png",$in_prefix,$i);
    $outfile = sprintf("%s%04d.png",$out_prefix,$i);
    print OUTFILE "$exec_name $infile $outfile\n";
}
close (OUTFILE); 
